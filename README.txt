'Sorting List' app made on React framework with 'create-react-app' package.

To start:
cd sorting-list
npm install
npm start

In app realized list of products in table form.
Table has pagination; counts of products to show;
opportunity to check products for delete; sorting by chosen field;
filtration by fields data.
Also there is creation form of new product with validation.

List of products is main page in app.
Another pages are category, product, about and error pages.
Routing made with 'react-router-dom' package.
On product page realized changing product info or removal of product.