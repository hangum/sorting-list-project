import React from 'react';


import './../../components/SearchPanel/css/custom.css';

/**
 * @props !array    allColors
 *
 * @props !fun_wrapper  titleCategoryFilter_WR
 * @props !fun_wrapper  priceFromFilter_WR
 * @props !fun_wrapper  priceToFilter_WR
 * @props !fun_wrapper  stockedFilter_WR
 * @props !fun_wrapper  colorFilter_WR
 * @props !fun_wrapper  checkedFilter_WR
 *
 * @props !string   titleCategoryText
 * @props !string   priceFrom
 * @props !string   priceTo
 * @props !string   color
 */
export default class SearchPanel extends React.Component{
    constructor(props){
        super(props);

        this.handleChangeTitleCategory = this.handleChangeTitleCategory.bind(this);
        this.handleChangePriceFrom = this.handleChangePriceFrom.bind(this);
        this.handleChangePriceTo = this.handleChangePriceTo.bind(this);
        this.handleCheckStocked = this.handleCheckStocked.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleShowCkecked = this.handleShowCkecked.bind(this);
    }

    handleChangeTitleCategory(event){
        this.props.titleCategoryFilter_WR(event.target.value)
    }
    handleChangePriceFrom(event){
        this.props.priceFromFilter_WR(event.target.value)
    }
    handleChangePriceTo(event) {
        this.props.priceToFilter_WR(event.target.value)
    }
    handleCheckStocked() {
        this.props.stockedFilter_WR(this.stokedCheck.checked);
    }
    handleChangeColor(event){
        this.props.colorFilter_WR(event.target.value);
    }
    handleShowCkecked(){
        this.props.checkedFilter_WR(this.showChecked.checked);
    }

    render() {
        let self = this;

        return (<form className="filter-form">
            <div className="col-sm-6 col-md-4" style={{paddingTop: '24px'}} >
                <label>Title/category filter</label>
                <input
                    type="text"
                    placeholder="Enter title or category"
                    className="form-control"
                    value={this.props.titleCategoryText}
                    onChange={this.handleChangeTitleCategory}
                />
            </div>
            <div className="col-sm-6 col-md-4" style={{paddingTop: '24px'}} >
                <label>Price filter</label>
                <br/>
                <input
                    type="text"
                    placeholder="from"
                    className="form-control"
                    style={{maxWidth: '48%', display: 'inline-block'}}
                    value={this.props.priceFrom}
                    onChange={this.handleChangePriceFrom}
                />
                <input
                    type="text"
                    placeholder="to"
                    style={{maxWidth: '48%', display: 'inline-block', float: 'right'}}
                    className="form-control"
                    value={this.props.priceTo}
                    onChange={this.handleChangePriceTo}
                />
            </div>
            <div className="col-sm-6 col-md-2" style={{paddingTop: '24px'}}>
                <label>Color filter</label>
                <select
                    className="form-control"
                    value={this.props.color}
                    onChange={this.handleChangeColor}
                >
                    <option value="">All</option>
                    {this.props.allColors.map(function(color){
                        return (<option value={color} key={color}>{color}</option>);
                    })}
                </select>
            </div>
            <div className="col-sm-6 col-md-2" style={{paddingTop: '24px'}}>
                <label>Check needed</label>
                <br/>
                <label>
                    <input
                        type="checkbox"
                        onChange={this.handleCheckStocked}
                        ref={function(elem){
                            self.stokedCheck = elem;
                        }}
                    />
                    {' '}Only on stock
                </label>
                <label>
                    <input
                        type="checkbox"
                        className="not-first"
                        onChange={this.handleShowCkecked}
                        ref={function(elem){
                            self.showChecked = elem;
                        }}
                    />
                    {' '}Only checked
                </label>
            </div>
        </form>);
    }
}