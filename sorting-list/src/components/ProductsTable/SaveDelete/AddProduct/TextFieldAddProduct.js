import React from 'react';


/**
 * @props !obj          data
 * @props !string       timerId
 * @props !fun_wrapper  changeField_WR
 * @props !fun_wrapper  validateField_WR
 * @props !fun_wrapper  timerValidate_WR
 */
export default function TextFieldAddProduct(props) {
    const data = props.data;

    function handleChange(event) {
        props.changeField_WR(data.name, event.target.value);

        props.timerValidate_WR(data.name, event.target.value);
    }

    function handleValidate(event){
        props.validateField_WR(data.name, event.target.value);
    }

    return (<div className="col-xs-12 col-sm-4 col-md-2 text-data">
        <label>
            {data.name}
            <input
                type="text"
                className="form-control"
                placeholder={"Enter " + data.name}
                onBlur = {handleValidate}
                onChange = {handleChange}
                value = {data.text}
            />
            {data.error &&
            <div className="error">{data.textError}</div>
            }
        </label>
    </div>);

}