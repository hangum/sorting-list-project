import React from 'react';


/**
 * @props !boolean      canSave
 * @props !obj          newProductData
 * @props !string       nextId
 *
 * @props !fun_wrapper  afterCreation_WR
 * @props !fun_wrapper  validateAll_WR
 * @props !fun_wrapper  saveNewProduct_WR
 */
export default function SaveProduct(props) {
    let btnClass = 'btn ';
    let iconClass = 'glyphicon ';
    if (props.canSave) {
        btnClass += 'btn-info';
        iconClass += 'glyphicon-floppy-disk';
    }
    else {
        btnClass += 'btn-warning';
        iconClass += 'glyphicon-remove';
    }

    function handleClick(event) {
        event.preventDefault();

        if (props.canSave) { //save new product
            let newProduct = {id: props.nextId};

            for (let fieldKey in props.newProductData) {
                if ( !props.newProductData.hasOwnProperty(fieldKey) ) continue;

                if (fieldKey === 'price') {
                    newProduct[fieldKey] ='$' + props.newProductData[fieldKey];
                }
                else {
                    newProduct[fieldKey] = props.newProductData[fieldKey];
                }
            }

            props.saveNewProduct_WR(newProduct);
            props.afterCreation_WR();
        }
        else {
            props.validateAll_WR(); //for showing errors
        }
    }

    return (<div className="col-xs-12" style={{paddingBottom: '12px'}}>
        <button
            className={btnClass}
            onClick = {handleClick}
        >
            <span className={iconClass} />
            <span style={{paddingLeft: '6px'}}>Save Product</span>
        </button>
    </div>)
}