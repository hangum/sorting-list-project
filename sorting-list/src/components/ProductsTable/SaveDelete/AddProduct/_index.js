import React from 'react';


import SaveProduct from './../../../../components/ProductsTable/SaveDelete/AddProduct/SaveProduct';
import TextFieldAddProduct from './../../../../components/ProductsTable/SaveDelete/AddProduct/TextFieldAddProduct';
import StockedRadio from './../../../../components/ProductsTable/SaveDelete/AddProduct/StockedRadio/_index';

import './../../../../components//ProductsTable/SaveDelete/AddProduct/css/custom.css';


//change state of input-text field
// objInState-->state.objInState - object in state object with params of input-text field
// objInState={name: 'fish', text: 'fish', error: boolean, textError: 'fish'}
// inputKey - some of keys for changing in objInState
// inputData - received data for objInState[inputKey]
export function changeFieldData(objInState, inputKey, inputData){
    for (let key in objInState) {
        if (key === inputKey && objInState.hasOwnProperty(key)) {
            objInState[key] = inputData;
        }
    }
    return objInState;
}


/**
 * @state !string    nextId
 * @state !string    timerId
 *
 * @state !obj       name
 * @state !obj       category
 * @state !obj       size
 * @state !obj       color
 * @state !obj       price
 * @state !boolean   stocked
 *
 * @props !string    nextId
 *
 * @props !fun_wrapper  saveNewProduct_WR
 * @props !fun_wrapper  showSuccessSave_WR
 */
export default class AddProduct extends  React.Component{
    constructor(props) {
        super(props);
        this.state = {
            timerId: null,

            name: {
                name: 'name',
                text: '',
                error: true,
                textError: ''
            },
            category: {
                name: 'category',
                text: '',
                error: true,
                textError: ''
            },
            size: {
                name: 'size',
                text: '',
                error: true,
                textError: ''
            },
            color: {
                name: 'color',
                text: '',
                error: true,
                textError: ''
            },
            price: {
                name: 'price',
                text: '',
                error: true,
                textError: ''
            },
            stocked: true
        };

        this.changeField = this.changeField.bind(this);
        this.changeRadio = this.changeRadio.bind(this);

        this.validateField = this.validateField.bind(this);
        this.validateAll = this.validateAll.bind(this);
        this.timerValidate = this.timerValidate.bind(this);

        this.afterCreation = this.afterCreation.bind(this);
    }

    afterCreation(){
        this.setState({stocked: true});

        //to begin states for name, category, size, color, price fields
        for (let inputKey in this.state){
            if (!this.state.hasOwnProperty(inputKey)
                ||
                ['name', 'category', 'size', 'color', 'price'].indexOf(inputKey) === -1
            ) {
                continue;
            }

            this.setState(function(prevState){
                return {
                    [inputKey]: changeFieldData(prevState[inputKey], 'text', ''),
                    [inputKey]: changeFieldData(prevState[inputKey], 'error', true),
                    [inputKey]: changeFieldData(prevState[inputKey], 'textError', '')
                };
            });
        }

        //modal window of success saving
        this.props.showSuccessSave_WR();
    }

    changeRadio(value){
        this.setState({stocked: value})
    }

    //change content in input-text fields
    // field - name, category, color, size, price
    changeField(field, inputText){
        this.setState(function(prevState) {
            return {
                [field]: changeFieldData(prevState[field], 'text', inputText)
            }
        });
    }

    //validation for input-text fields
    // field - name, category, color, size, price
    validateField(field, inputText) {
        if (inputText.length === 0) {
            this.setState(function(prevState) {
                return {
                    [field]: changeFieldData(prevState[field], 'error', true),
                    [field]: changeFieldData(prevState[field], 'textError', 'Enter ' + field)
                }
            });
        }
        else if (
            ['name', 'category', 'color'].indexOf(field) !== -1
            &&
            inputText.length < 3
        ) {
            this.setState(function(prevState) {
                return {
                    [field]: changeFieldData(prevState[field], 'error', true),
                    [field]: changeFieldData(prevState[field], 'textError', 'Enter more then 2 symbols')
                }
            });
        }
        else if (field === 'price' && isNaN(inputText) ) {
            this.setState(function(prevState) {
                return {
                    [field]: changeFieldData(prevState[field], 'error', true),
                    [field]: changeFieldData(prevState[field], 'textError', 'Enter number')
                }
            });
        }
        else { //success validation
            this.setState(function(prevState) {
                return {
                    [field]: changeFieldData(prevState[field], 'error', false),
                    [field]: changeFieldData(prevState[field], 'textError', null)
                }
            });
        }
    }

    validateAll(){
        this.validateField('name', this.state.name.text);
        this.validateField('category', this.state.category.text);
        this.validateField('size', this.state.size.text);
        this.validateField('color', this.state.color.text);
        this.validateField('price', this.state.price.text);
    }

    timerValidate(field, inputText){ //validation for onChange-event of <input> value
        let validateField = this.validateField.bind(this, field, inputText);

        // clear timeout for validation,
        // if focus on other <input> - work validation for onBlur-event
        if (this.state.timerId) {
            clearTimeout(this.state.timerId);
        }

        this.setState({timerId: setTimeout(function(){
            validateField();
        }, 1500)});
    }

    render(){
        let canSave = true; //toggle view of button "save product"
        let newProductData = {}; //data for saving new product
        for (let inputField in this.state){
            if ( this.state.hasOwnProperty(inputField) ){
                if (['name', 'category', 'size', 'color', 'price'].indexOf(inputField) !== -1) {
                    if (this.state[inputField].error === true) {
                        canSave = false;
                        break;
                    }
                    else {
                        newProductData[inputField] = this.state[inputField].text;
                    }
                }
                else if ( inputField === 'stocked') {
                    newProductData[inputField] = this.state[inputField];
                }
            }
        }

        return (<form className="add-product-form row">
            <SaveProduct
                canSave = {canSave}
                newProductData = {newProductData}
                nextId = {this.props.nextId}
                afterCreation_WR = {this.afterCreation}
                validateAll_WR = {this.validateAll}
                showModal = {this.state.showModal}
                closeModal_WR = {this.closeModal}
                saveNewProduct_WR = {this.props.saveNewProduct_WR}
            />

            <TextFieldAddProduct
                data = {this.state.name}
                changeField_WR = {this.changeField}
                validateField_WR = {this.validateField}
                timerValidate_WR = {this.timerValidate}
            />
            <TextFieldAddProduct
                data = {this.state.category}
                changeField_WR = {this.changeField}
                validateField_WR = {this.validateField}
                timerValidate_WR = {this.timerValidate}
            />
            <TextFieldAddProduct
                data = {this.state.size}
                changeField_WR = {this.changeField}
                validateField_WR = {this.validateField}
                timerValidate_WR = {this.timerValidate}
            />
            <TextFieldAddProduct
                data = {this.state.color}
                changeField_WR = {this.changeField}
                validateField_WR = {this.validateField}
                timerValidate_WR = {this.timerValidate}
            />
            <TextFieldAddProduct
                data = {this.state.price}
                changeField_WR = {this.changeField}
                validateField_WR = {this.validateField}
                timerValidate_WR = {this.timerValidate}
            />
            <StockedRadio
                typeClass="column"
                stocked = {this.state.stocked}
                changeRadio_WR = {this.changeRadio}
            />
        </form>);
    }

}