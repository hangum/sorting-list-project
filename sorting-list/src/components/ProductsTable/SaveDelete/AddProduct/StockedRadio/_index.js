import React from 'react';

import './css/custom.css';

/** *
 * @props !boolean      stocked
 * @props !string       typeClass
 * @props !fun_wrapper  changeRadio_WR
 */
export default function StockedRadio(props){

    function handleChange(event) {
        if (
            ( event.currentTarget.dataset.type === 'stocked'  &&  !props.stocked )
            ||
            ( event.currentTarget.dataset.type === 'outOfStock'  &&  props.stocked )
        ) {
            props.changeRadio_WR( !props.stocked );
        }
    }

    let className;
    if (props.typeClass === "column") {
        className = "col-xs-12 col-sm-4 col-md-2 column-radio-group";
    }
    else {
        className = "row-radio-group";
    }

    return (
        <div className={className} >
            <label>
                <input
                    type="radio"
                    data-type="stocked"
                    checked={ props.stocked }
                    onChange={ handleChange }
                />
                <span>Stocked</span>
            </label>
            <label>
                <input
                    type="radio"
                    data-type="outOfStock"
                    checked={ !props.stocked  }
                    onChange={ handleChange }
                />
                <span>Out of stock</span>
            </label>
        </div>);
}
