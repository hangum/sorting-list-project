import React from 'react';


/**
 * @props !array        checkedPageId
 * @props !boolean      toPreviousPage
 * @props !fun_wrapper  multiDeleteProducts_WR
 */
export default function DeleteProducts(props) {

    function handleClick(event) {
        props.multiDeleteProducts_WR(props.checkedPageId, props.toPreviousPage);
    }

    return (<button
        className="btn btn-danger"
        style={{marginTop: '12px', width: '174px'}}
        onClick={handleClick}
    >
        Delete chosen product{ props.checkedPageId.length > 1 && 's' }
    </button>);
}