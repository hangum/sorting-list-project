import React from 'react';


import DeleteProducts from './../../../components/ProductsTable/SaveDelete/DeleteProducts';
import AddProduct from './../../../components/ProductsTable/SaveDelete/AddProduct/_index';

/**
 * @state !string       open
 *
 * @props !string       nextId
 * @props !array        checkedPageId
 * @props !boolean      toPreviousPage
 *
 * @props !fun_wrapper  multiDeleteProducts_WR
 * @props !fun_wrapper  showSuccessSave_WR
 * @props !fun_wrapper  saveNewProduct_WR
 */
export default class SaveDelete extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) { //open new products form
        this.setState(function(prevState) {
            return {open: !prevState.open};
        });
    }

    render() {
        return (<div>
            <button
                className="btn btn-info"
                style={{marginTop: '12px', marginRight: '12px', width: '174px'}}
                onClick = {this.handleClick}
            >
                {this.state.open ? 'Close Form' : 'Add Product'}
            </button>

            { this.props.checkedPageId.length > 0 &&
            <DeleteProducts
                checkedPageId={this.props.checkedPageId}
                multiDeleteProducts_WR={this.props.multiDeleteProducts_WR}
                toPreviousPage={this.props.toPreviousPage}
            />
            }

            { this.state.open &&
            <AddProduct
                nextId = {this.props.nextId}
                saveNewProduct_WR = {this.props.saveNewProduct_WR}
                showSuccessSave_WR = {this.props.showSuccessSave_WR}
            />
            }
        </div>);
    }
}