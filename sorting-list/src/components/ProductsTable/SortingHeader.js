import React from 'react';


/**
 * @props !string       sortingField
 * @props !string       orderBy
 * @props !obj          nameFields
 * @props !array        idPaginationProducts
 * @props !fun_wrapper  multiCheckRows_WR
 * @props !fun_wrapper  changeSorting_WR
 */
export default class SortingHeader extends React.Component {
    constructor(props){
        super(props);

        this.handleMultiCheck = this.handleMultiCheck.bind(this);
        this.handleClickSort = this.handleClickSort.bind(this);
    }
    handleMultiCheck(event){
        this.props.multiCheckRows_WR(this.props.idPaginationProducts);
    }
    handleClickSort(event){
        let newSortingField = event.target.closest('th').dataset.sortingField;
        if (newSortingField) {
            this.props.changeSorting_WR(newSortingField);
        }
    }
    render() {
        const nameFields = this.props.nameFields;
        const sortingField = this.props.sortingField;
        const orderBy = this.props.orderBy;

        let row = [];
        for (let key in nameFields) {
            if ( !nameFields.hasOwnProperty(key) || key ==='stocked' ) break;

            let typeSort = null;
            if (key === sortingField) {
                if (orderBy ==='asc') {
                    typeSort = <span className="glyphicon glyphicon-menu-up arrow-asc" />;
                }
                else if (orderBy ==='desc') {
                    typeSort = <span className="glyphicon glyphicon-menu-up arrow-desc" />;
                }
            }

            row.push(<th
                className={ (key === sortingField) ? "sortingField head" : "head" }
                key={key}

                data-sorting-field = {key}
                onClick = {this.handleClickSort}
            >
                {key}
                {typeSort}
            </th>);
        }

        return (<tr>
            <th>
                <span
                    style={{cursor: 'pointer'}}
                    className="glyphicon glyphicon-ok"
                    onClick = {this.handleMultiCheck}
                />
            </th>
            {row}
        </tr>);
    }
}