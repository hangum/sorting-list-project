import React from 'react';


/**
 * @props !string   pageNumber
 * @props !string   navigationType
 * @props !boolean  activePage     //for css
 * @props !boolean  status
 */
export default function PageNumber(props) {

    function handleClick(event){
        event.preventDefault();
        props.changeCurrentPage_WR(event.target.closest('a').dataset.number);
    }

    let contentLi;
    switch (props.navigationType) {
        case 'pageNumber' :
            contentLi = <span>{props.pageNumber}</span>;
            break;
        case 'prev' :
            contentLi = <span className="glyphicon glyphicon-chevron-left" />;
            break;
        case 'next' :
            contentLi = <span className="glyphicon glyphicon-chevron-right" />;
            break;
        default: break;
    }

    let classLi = (props.activePage === true ) ? "active" : "";

    let link = null;
    if (props.status) {
        link = <a
            data-number={props.pageNumber}
            onClick={handleClick}
            className={classLi}
        >
            {contentLi}
        </a>;
    }
    else {
        link = <a className={classLi}>{contentLi}</a>
    }

    return (<li>{link}</li>);
}