import React from 'react';


import ListOfPages from './../../../components/ProductsTable/Paginator/ListOfPages';
import CounterToShow from './../../../components/ProductsTable/Paginator/CounterToShow';

import './../../../components/ProductsTable/Paginator/css/custom.css';

/**
 * @props !string      countPages
 * @props !string      countsToShow
 * @props !string      currentPage
 * @props !fun_wrapper changeCountsToShow_WR
 * @props !fun_wrapper changeCurrentPage_WR
 */
export default function Paginator(props) {

    return (<div className="choice-pages-block">
        { ((props.countsToShow !== 'all') && (props.countPages > 1) )
            ?
            <ListOfPages
                countsPassedFiltration ={props.countsPassedFiltration}
                countsToShow = {props.countsToShow}
                currentPage = {props.currentPage}
                countPages = {props.countPages}
                changeCurrentPage_WR = {props.changeCurrentPage_WR}
            />
            :
            <div></div>
        }
        <CounterToShow
            countsToShow={props.countsToShow}
            changeCountsToShow_WR={props.changeCountsToShow_WR}
        />
    </div>);
}