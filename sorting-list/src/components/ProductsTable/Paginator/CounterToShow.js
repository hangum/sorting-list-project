import React from 'react';


/**
 * @props !string       countsToShow
 * @props !fun_wrapper  changeCountsToShow_WR
 */
export default function CounterToShow(props) {

    function  handleChange(event){
        props.changeCountsToShow_WR(event.target.value);
    }

    return (<div>
        <select
            className="counts-to-show"
            value={props.countsToShow}
            onChange={handleChange}
        >
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="40">40</option>
            <option value="all">All</option>
        </select>
    </div>);
}