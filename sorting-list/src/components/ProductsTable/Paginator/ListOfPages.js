import React from 'react';


import PageNumber from './../../../components/ProductsTable/Paginator/PageNumber';

/**
 * @props !string      countsPassedFiltration
 * @props !string      countsToShow
 * @props !string      currentPage
 * @props !string      countPages
 * @props !fun_wrapper changeCurrentPage_WR
 */
export default function ListOfPages(props) {

    const currentPage = +props.currentPage;
    const countPages = +props.countPages;

    //components with number of pages
    let pagesNumbers = [];
    for (let j=1; j < countPages+1; j++){
        let activePage = ( j === currentPage);

        function pushNumber(){ //for number page
            pagesNumbers.push(
                <PageNumber
                    key = {j}
                    pageNumber = {j}
                    navigationType = 'pageNumber'
                    status = {!activePage}
                    activePage = {activePage}
                    changeCurrentPage_WR = {props.changeCurrentPage_WR}
                />
            );
        }
        function pushThreeDots(){ //for threeDots
            pagesNumbers.push(
                <li key={j} >
                    <a><span>...</span></a>
                </li>
            );
        }

        if (countPages > 6) { //cut some pages, add threeDots

            if ( j === 1 || j === countPages) { //first, last
                pushNumber();
            }
            else if (j === 2) { //prefirst
                if (currentPage > 3 && currentPage !== 1) {
                    pushThreeDots();
                }
                else {
                    pushNumber();
                }
            }
            else if (j === 3) { //preprefirst
                if (currentPage < 5 ) {
                    pushNumber();
                }
            }
            else if (j === countPages-1) { //prelast
                if (currentPage < countPages-2) {
                    pushThreeDots();
                }
                else {
                    pushNumber();
                }
            }
            else if (j === countPages-2) { //preprelast
                if (currentPage > countPages-4 ) {
                    pushNumber();
                }
            }
            else if ( j > 2 && j < countPages-1 ) { //in central part of list
                if ( j <= currentPage+2 && j >= currentPage-2) {
                    pushNumber();
                }
            }

        }
        else { //all pages show
            pushNumber();
        }
    }

    let backBtnStatus = (currentPage !== 1);
    let nextBtnStatus = (currentPage !== countPages);

    return (<div>
        <ul className="pagination pagination-sm" >
            <PageNumber
                key='prev'
                pageNumber={currentPage - 1}
                navigationType='prev'
                status={backBtnStatus}
                activePage='false'
                changeCurrentPage_WR={props.changeCurrentPage_WR}
            />

            {pagesNumbers}

            <PageNumber
                key='next'
                pageNumber={currentPage + 1}
                navigationType='next'
                status={nextBtnStatus}
                activePage='false'
                changeCurrentPage_WR={props.changeCurrentPage_WR}
            />
        </ul>
    </div>);

}