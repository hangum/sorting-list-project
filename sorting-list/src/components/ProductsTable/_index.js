import React from 'react';

import SaveDelete from './../../components/ProductsTable/SaveDelete/_index';
import Paginator from './../../components/ProductsTable/Paginator/_index';
import SortingHeader from './../../components/ProductsTable/SortingHeader';
import ProductRow from './../../components/ProductsTable/ProductRow';

import './../../components/ProductsTable/css/custom.css';

//if fields in sorting rows are equal,
// sorting this rows by id
function sortingWithId (
    fieldA, fieldB,
    idA, idB,
    orderBy='asc'
) {
    if (orderBy === 'asc'){
        return (fieldB<fieldA) - (fieldA<fieldB) || (idB<idA) - (idA<idB);
    }
    else if (orderBy === 'desc'){
        return (fieldA<fieldB) - (fieldB<fieldA) || (idA<idB) - (idB<idA);
    }

}

/**
 * @state !string       sortingField
 * @state !string       orderBy
 *
 * @props !array        products
 * @props !string       nextId
 * @props !array        checkedId
 *
 * @props !fun_wrapper  checkRow_WR
 * @props !fun_wrapper  multiCheckRows_WR
 *
 * @props !string       titleCategoryText
 * @props !string       priceFrom
 * @props !string       priceTo
 * @props !boolean      stocked
 * @props !string       color
 * @props !boolean      onlyChecked
 *
 * @props !string       countsToShow
 * @props !string       currentPage
 *
 * @props !fun_wrapper  changeCountsToShow_WR
 * @props !fun_wrapper  changeCurrentPage_WR
 *
 * @props !fun_wrapper  multiDeleteProducts_WR
 * @props !fun_wrapper  saveNewProduct_WR
 *
 * @props !fun_wrapper  showSuccessSave_WR
 */
export default class ProductsTable extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            sortingField: 'id',
            orderBy: 'asc'
        };

        this.changeSorting = this.changeSorting.bind(this);
    }

    changeSorting(fieldName) {
        if (fieldName === this.state.sortingField) {
            this.setState({orderBy: (this.state.orderBy === 'asc') ? 'desc' : 'asc'});
        }
        else {
            this.setState({sortingField: fieldName, orderBy: 'asc'});
        }
    }

    render() {
        let self = this;
        let products = this.props.products;

        const checkedId = self.props.checkedId;

        // sorting vars
        const sortingField = self.state.sortingField;
        const orderBy = self.state.orderBy;

        // filter vars
        const titleCategoryText = self.props.titleCategoryText;
        const priceFrom = self.props.priceFrom;
        const priceTo = self.props.priceTo;
        const stocked = self.props.stocked;
        const color = self.props.color;
        const onlyChecked = self.props.onlyChecked;

        let nameFields = {};  //name fields for SortingHeader
        for (let key in products[0]){
            if ( products[0].hasOwnProperty(key) ) {
                nameFields[key] = key;
            }
        }

        //filtering by input params
        let filteringProducts = products.filter( function(product) {
            let filtering = {
                need: 5, // count of all conditions to show product
                have: [] // conditions names
            };

            for (let key in product) {
                switch (key) {
                    case 'id' :
                        if (!onlyChecked) {
                            filtering.have.push('onlyChecked');
                        }
                        else if ( onlyChecked && checkedId.indexOf(product.id) !== -1) {
                            filtering.have.push('onlyChecked');
                        }
                        break;
                    case 'name' :
                        if (product[key].toLowerCase().indexOf(titleCategoryText.toLowerCase()) !== -1) {
                            filtering.have.push('name/category');
                        }
                        break;
                    case 'category' :
                        if (filtering.have.indexOf('name/category') !== -1) {
                            break;
                        }
                        else if (product[key].toLowerCase().indexOf(titleCategoryText.toLowerCase()) !== -1) {
                            filtering.have.push('name/category');
                        }
                        break;
                    case 'price' :
                        let productPrice = +product[key].slice(1); //delete "$"

                        if ( (priceFrom === '') && (priceTo === '') ){
                            filtering.have.push('price');
                        }
                        else if ( (priceFrom === '') && (productPrice <= priceTo) ){
                            filtering.have.push('price');
                        }
                        else if ( (priceTo === '') && (productPrice >= priceFrom) ){
                            filtering.have.push('price');
                        }
                        //::TODO xz why (productPrice >= +priceFrom) compare like string
                        else if ( (productPrice - priceFrom >= 0) && (productPrice - priceTo <= 0) ){
                            filtering.have.push('price');
                        }
                        break;
                    case 'stocked' :
                        if ( !stocked) {
                            filtering.have.push('stocked');
                        }
                        else if (stocked && product[key] ) {
                            filtering.have.push('stocked');
                        }
                        break;
                    case 'color' :
                        if (color === '') { // for all colors
                            filtering.have.push('color');
                        }
                        else if (product[key] === color) {
                            filtering.have.push('color');
                        }
                        break;
                    default: break;
                }
            }

            if ( filtering.need === filtering.have.length) {
                return product;
            }
            return undefined;
        });

        //sorting by chosen field
        filteringProducts.sort( function(a,b){
            if ( ['price'].indexOf(sortingField) !== -1 ) { //price field
                //delete "$" for sorting price string
                const priceA = +a[sortingField].slice(1);
                const priceB = +b[sortingField].slice(1);

                return sortingWithId(
                    priceA, priceB,
                    a['id'], b['id'],
                    orderBy
                );
            }
            else { //other fields
                return sortingWithId(
                    a[sortingField], b[sortingField],
                    a['id'], b['id'],
                    orderBy
                );
            }
        });

        // border for shown products by pagination
        let showFrom = 0;
        let showTo = Infinity;
        if (this.props.countsToShow !== 'all') {
            showFrom = (this.props.currentPage - 1) * this.props.countsToShow ;
            showTo = this.props.currentPage * this.props.countsToShow;
        }
        // products in pagination border
        let idPaginationProducts =[]; //for multi check
        const paginationProducts = filteringProducts.filter(function(product, key) {
            if (( key >= showFrom) && (key < showTo) ) {
                idPaginationProducts.push(product.id);
                return product;
            }
            return undefined;
        });

        //checkedPageId - id products, which checked on current page
        let checkedPageId = [];

        //make array of showing products. saving id, checked on current page
        const rows = paginationProducts.map(function(product) {
            let checked = false;
            if (checkedId.indexOf(product.id) !== -1 ) {
                checked = true;
                checkedPageId.push(product.id);
            }

            return (<ProductRow
                key={product.id}
                product={product}
                sortingField={sortingField}
                checked = {checked}
                checkRow_WR = {self.props.checkRow_WR}
            />);
        });

        // count of showing pages
        const countPages = Math.ceil(filteringProducts.length / this.props.countsToShow);

        // toPreviousPage - pointer of transition to previous page,
        // if delete all products on current page and this page is last
        const toPreviousPage = (
            (checkedPageId.length === rows.length)
            && (+this.props.currentPage === countPages)
            && (countPages !== 1)
        );

        return (<div>
            <SaveDelete
                checkedPageId = {checkedPageId}
                nextId = {this.props.nextId}
                toPreviousPage = {toPreviousPage}
                multiDeleteProducts_WR = {this.props.multiDeleteProducts_WR}
                showSuccessSave_WR = {this.props.showSuccessSave_WR}
                saveNewProduct_WR = {this.props.saveNewProduct_WR}
            />

            <Paginator
                countPages = {countPages}
                countsToShow = {this.props.countsToShow}
                currentPage = {this.props.currentPage}
                changeCountsToShow_WR = {this.props.changeCountsToShow_WR}
                changeCurrentPage_WR = {this.props.changeCurrentPage_WR}
            />
            <div className="table-responsive">
                <table className="table products-table">
                    <thead>
                    <SortingHeader
                        sortingField={sortingField}
                        orderBy={orderBy}
                        nameFields = {nameFields}
                        idPaginationProducts = {idPaginationProducts}
                        changeSorting_WR = {this.changeSorting}
                        multiCheckRows_WR = {this.props.multiCheckRows_WR}
                    />
                    </thead>
                    <tbody>
                    {rows}
                    </tbody>
                </table>
            </div>
        </div>);
    }
}