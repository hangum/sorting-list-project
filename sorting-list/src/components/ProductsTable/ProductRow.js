import React from 'react';

import {Link} from 'react-router-dom';

import './../../components/ProductsTable/css/custom.css';

/**
 * @props !obj          product
 * @props !string       sortingField
 * @props !boolean      checked
 * @props !fun_wrapper  checkRow_WR
 */
export default function ProductRow(props){

    function handleChange(event){
        props.checkRow_WR(event.target.value);
    }

    const product = props.product;
    const sortingField = props.sortingField;
    const checked = props.checked;

    let color;
    let linkColor;
    if (!product.stocked) {
        color = 'red';
        linkColor = '#dd0680';
    }
    else {
        color = '';
        linkColor = '#143d7f';
    }

    let row = [];
    let bgCheckColor = '';
    for (let key in product) {
        //skip "stocked" field, which hasn`t own field in table
        if ( !product.hasOwnProperty(key) || key ==='stocked' ) break;

        let bgColor; //field bg-color
        if (checked && key === sortingField) {
            bgColor = 'sortingCheckedField';
            bgCheckColor = 'checkedField';
        }
        else if (checked && key !==sortingField) {
            bgColor = 'checkedField';
            bgCheckColor = 'checkedField';
        }
        else if (!checked && key ===sortingField) {
            bgColor = 'sortingField';
        }
        else {
            bgColor = '';
        }

        let cellContent;
        switch (key) {
            case 'name':
                cellContent = (<Link
                    style={{color: linkColor}}
                    to={`/products/${product.category}/${product.id}`}
                >
                    {product.name}
                </Link>);
                break;
            case 'category':
                cellContent = (<Link
                    style={{color: linkColor}}
                    to={`/products/${product.category}`}
                >
                    {product.category}
                </Link>);
                break;
            default:
                cellContent = product[key];
        }
        row.push(
            <td className={bgColor} key={key} >
                {cellContent}
            </td>
        );
    }

    return (
        <tr style={ {color: color} }>
            <td className={bgCheckColor} >
                <input
                    style={{cursor: 'pointer'}}
                    type="checkbox"
                    value={product.id}
                    onChange={handleChange}
                    checked={checked}
                />
            </td>
            {row}
        </tr>
    );
}