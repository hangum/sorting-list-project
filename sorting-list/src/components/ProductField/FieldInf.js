import React from 'react';

/**
 * @props !string field
 * @props !string value
 */
export default function FieldInf(props) {
    const field = props.field;
    const value = props.value;

    let infoContent;
    if ( field ==='stocked') {
        infoContent = value ? "Stocked" : "Out of stock";
    }
    else if (field !== 'id') {
        infoContent = value;
    }

    return ( <div>
        <span>{field}: </span>
        <span className="field-info" >{infoContent}</span>
    </div>);
}