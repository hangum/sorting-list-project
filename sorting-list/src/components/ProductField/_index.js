import React from 'react';

import FieldInf from './FieldInf';
import ToggleBtn from './ToggleBtn';
import SaveChangeBtn from './SaveChangeBtn';
import StockedRadio from "./../../components/ProductsTable/SaveDelete/AddProduct/StockedRadio/_index";

import "./css/custom.css";

/**
 * @state !string/boolean   newValue
 * @state !text             errorText
 *
 * @props !string/boolean   value (for field=='stocked' is boolean)
 * @props !string           field
 * @props !string           id
 *
 * @props !boolean          openInput
 * @props !fun_wrapper      setActiveField_WR
 * @props !fun_wrapper      editProduct_WR
 *
 * ?? @props !fun_wrapper   updateLinkCategory_WR (only when props.field=='category')
 */
export default class ProductField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newValue: null,
            errorText: ''
        };

        this.handleToggleInput = this.handleToggleInput.bind(this);
        this.changeRadio = this.changeRadio.bind(this);
        this.changeInputText = this.changeInputText.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    componentDidMount(){
        this.setState(function(prevState, props) {
            // delete "$"
            let newValue = (props.field === 'price') ? props.value.slice(1) : props.value;

            return {newValue: newValue};
        })
    }

    handleToggleInput() {
        if ( !this.props.openInput ) {
            this.props.setActiveField_WR(this.props.field);
        }
        else {
            this.props.setActiveField_WR(null);
        }
    }

    changeRadio(value){
        this.setState({newValue: value})
    }
    changeInputText(event){
        this.setState({newValue: event.target.value})
    }

    handleSave(){
        let field = this.props.field;
        let newValue = this.state.newValue;

        let errorText = '';
        if (newValue.length === 0) {
            errorText = 'Enter ' + field;
        }
        else if (
            ['name', 'category', 'color'].indexOf(field) !== -1
            &&
            newValue.length < 3
        ) {
            errorText = 'Enter more then 2 symbols';
        }
        else if (field === 'price' && isNaN(newValue) ) {
            errorText = 'Enter number';
        }

        //save if validation successful
        if ( errorText === '') {
            this.props.editProduct_WR(this.props.id, field, newValue);
            this.handleToggleInput();

            //change link, url when change category
            if (this.props.field === 'category' && this.props.updateLinkCategory_WR) {
                this.props.updateLinkCategory_WR(newValue, this.props.id);
            }
        }

        this.setState({errorText: errorText});
    }

    render() {
        const field = this.props.field;
        const value = this.props.value;
        const open = this.props.openInput;

        let inputElem = null;
        if (open) {
            if ( field ==='stocked') {
                inputElem = (<StockedRadio
                    typeClass="row"
                    stocked={this.state.newValue}
                    changeRadio_WR = {this.changeRadio}
                />);
            }
            else if (field !== 'id') {
                inputElem = (<input
                    className="form-control"
                    type="text"
                    value={this.state.newValue}
                    onChange={this.changeInputText}
                />);
            }
        }

        //condition to show save btn
        let showSaveBtn = false;
        if (this.state.errorText === '') {
            if (open && value !== this.state.newValue) {
                showSaveBtn = true;
            }
        }
        else {
            if (open) showSaveBtn = true;
        }

        return (<li className="list-group-item">
            <div className="flex-row">
                <FieldInf
                    field ={this.props.field}
                    value = {this.props.value}
                />
                <div>
                    { showSaveBtn &&
                    <SaveChangeBtn
                        errorText = {this.state.errorText}
                        handleSave_WR = {this.handleSave}
                    />}
                    <ToggleBtn
                        open = {open}
                        handleToggleInput_WR = {this.handleToggleInput}
                    />
                </div>
            </div>

            {inputElem}

            { open &&
            <div style={{color: "red", fontSize: '13px'}}>{this.state.errorText}</div>
            }
        </li>);
    }
}