import React from 'react';

/**
 * @props !string       errorText
 * @props !fun_wrapper  handleSave_WR
 */
export default function SaveChangeBtn(props){
    let btnClass = "btn ";
    let iconClass = "glyphicon ";

    if (props.errorText === '') {
        btnClass += "btn-success";
        iconClass += "glyphicon-ok";
    }
    else {
        btnClass += "btn-danger";
        iconClass += "glyphicon-flash";
    }

    return (<button
        className = {btnClass}
        onClick = {props.handleSave_WR}
    >
        <span className = {iconClass} />
    </button>);
}