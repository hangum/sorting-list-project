import React from 'react';

/**
 * @props !boolean      open
 * @props !fun_wrapper  handleToggleInput_WR
 */
export default function ToggleBtn(props){
    let btnClass = "btn ";
    let iconClass = "glyphicon ";

    if ( !props.open ) {
        btnClass += "btn-info";
        iconClass += "glyphicon-edit";
    }
    else {
        btnClass += "btn-warning";
        iconClass += "glyphicon-remove";
    }

    return (<button
        className = {btnClass}
        onClick = {props.handleToggleInput_WR}
    >
        <span className={iconClass} />
    </button>);
}