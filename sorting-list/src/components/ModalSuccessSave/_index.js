import React from 'react';

import "./../../components/ModalSuccessSave/css/custom.css"

/**
 * @props !fun_wrapper closeSuccessSave_WR
 */
export default function ModalSuccessSave(props){
    function handleClick(event){
        props.closeSuccessSave_WR();
    }

    return (<div className="modal-success-save">
        <div className="modal-body">
            <div className="close-cross">
                <span
                    className="glyphicon glyphicon-remove"
                    onClick = {handleClick}
                />
            </div>
            <h3>New product successfully saved</h3>
            <button
                className="btn btn-info"
                onClick={handleClick}
            >
                Close window
            </button>
        </div>
    </div>);
}