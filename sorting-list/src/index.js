/**
 *  List with sorting
 * */

// Box Components
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

// Router Components
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link
} from 'react-router-dom';

//custom Components of pages
import MainPage from './views/MainPage/index';
import AboutPage from './views/AboutPage/index';
import CategoryPage from './views/CategoryPage/index';
import ProductPage from './views/ProductPage/index';
import ErrorPage from './views/ErrorPage/index';

// CSS-styles, CSS-libraries
import './libs/bootstrap/css/bootstrap.css'; //bootstrap
import './css/custom.css';                   //custom CSS

//data vars
import {PRODUCTS} from './dataFiles/productsData';
import {nextId} from './dataFiles/productsData';


/**
 * @state !array    products
 * @state !string   nextId
 * @state !array    checkedId
 *
 * @state !sting    titleCategoryText
 * @state !string   priceFrom
 * @state !string   priceTo
 * @state !boolean  stocked
 * @state !string   color
 * @state !boolean  onlyChecked
 *
 * @state !string   countsToShow
 * @state !string   currentPage
 *
 */
class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            products: PRODUCTS,
            nextId: nextId,
            checkedId: [],

            //filtering states
            titleCategoryText: '',
            priceFrom: '',
            priceTo: '',
            stocked: false,
            color: '', //value for all colors
            onlyChecked: false,

            //pagination states
            countsToShow: "10",
            currentPage: "1"
        };

        //wrappers checking rows
        this.checkRow = this.checkRow.bind(this);
        this.multiCheckRows = this.multiCheckRows.bind(this);

        //wrappers filtering rows
        this.titleCategoryFilter = this.titleCategoryFilter.bind(this);
        this.priceFromFilter = this.priceFromFilter.bind(this);
        this.priceToFilter = this.priceToFilter.bind(this);
        this.stockedFilter = this.stockedFilter.bind(this);
        this.colorFilter = this.colorFilter.bind(this);
        this.checkedFilter = this.checkedFilter.bind(this);

        //wrappers for pagination
        this.changeCountsToShow = this.changeCountsToShow.bind(this);
        this.changeCurrentPage = this.changeCurrentPage.bind(this);

        //wrapper for delete products
        this.multiDeleteProducts = this.multiDeleteProducts.bind(this);
        this.SingleDeleteProduct = this.SingleDeleteProduct.bind(this);

        //wrapper for save new product
        this.saveNewProduct = this.saveNewProduct.bind(this);

        //wrapper for edit product
        this.editProduct = this.editProduct.bind(this);
    }

    titleCategoryFilter(text){
        this.setState({
            titleCategoryText: text,
            currentPage: "1"
        });
    }
    priceFromFilter(from) {
        if ( !isNaN(from) ){
            this.setState({
                priceFrom: from,
                currentPage: "1"
            })
        }
    }
    priceToFilter(to) {
        if ( !isNaN(to) ) {
            this.setState({
                priceTo: to,
                currentPage: "1"
            })
        }
    }
    stockedFilter(stocked) {
        this.setState({
            stocked: stocked,
            currentPage: "1"
        })
    }
    colorFilter(color) {
        this.setState({
            color: color,
            currentPage: "1"
        })
    }
    checkedFilter(value) {
        this.setState({
            onlyChecked: value,
            currentPage: "1"
        })
    }


    changeCountsToShow(counts){
        this.setState({
            countsToShow: counts,
            currentPage: "1"
        });
    }
    changeCurrentPage(value){
        this.setState({currentPage: value})
    }


    checkRow(id) {
        id = +id; //to number
        const key = this.state.checkedId.indexOf(id);
        if (key === -1) {
            this.setState(function(prevState){
                return {checkedId: prevState.checkedId.concat(id)};
            });
        }
        else {
            this.setState(function(prevState){
                return {checkedId: prevState.checkedId.filter(
                    function(val){
                        return id !== val;
                    })
                };
            });
        }
    }
    multiCheckRows(arr){
        const self = this;

        let coincide = 0;
        this.state.checkedId.forEach(function(stateId){
            arr.forEach(function(checkedId) {
                if ( +stateId === +checkedId ) {
                    coincide++;
                }
            })
        });

        if ( coincide === arr.length) { //uncheck
            arr.forEach(function(id) {
                const key = self.state.checkedId.indexOf(id);
                if ( key !== -1) {
                    self.setState( function(prevState){
                        return {checkedId: prevState.checkedId.filter(
                            function(val){
                                return id !== val;
                            })
                        };
                    });
                }
            });
        }
        else { //check
            arr.forEach(function(id) {
                const key = self.state.checkedId.indexOf(id);
                if (key === -1) {
                    self.setState(function(prevState){
                        return {checkedId: prevState.checkedId.concat(id)};
                    });
                }
            });
        }
    }

    multiDeleteProducts(arrayId, toPreviousPage) {

        //clean deleting product id (checkedId) from state
        this.setState(function(prevState){
            return { checkedId: prevState.checkedId.filter(
                function(idInState) {
                    if ( arrayId.indexOf(idInState) !== -1) {
                        return undefined;
                    }
                    return idInState;
                }
            )}
        });

        //delete checking products from products
        function filterByDelete(arrDelId){ // arrDelId - array of products to delete
            return function(objProduct) { //callback function for .filter method
                if (arrDelId.indexOf(objProduct.id) === -1){
                    return objProduct;
                }
                return undefined;
            }
        }
        const filterWrapper = filterByDelete.bind(this, arrayId);
        this.setState(function(prevState){
            return {
                products: prevState.products.filter( filterWrapper() )
            };
        });

        //transition to previous page if current is last and empty after deleting
        if (toPreviousPage) {
            this.setState(function(prevState){
                return {currentPage: prevState.currentPage - 1};
            });
        }
    }

    SingleDeleteProduct(id){
        id = +id;
        this.setState(function(prevState) {
            return {
                products: prevState.products.filter( function(product) {
                    return product.id !== id;
                }),
                checkedId: prevState.checkedId.filter(function(oneCheckedId) {
                    return oneCheckedId !== id;
                })
            };
        });
    }

    saveNewProduct(objProduct){
        this.setState(function(prevState){
            return {
                products: prevState.products.concat(objProduct),
                nextId: +prevState.nextId + 1
            }
        });
    }

    editProduct(id, field, value) {
        this.setState(function(prevState) {
            return { products: prevState.products.map(function(product) {
                if (product.id === id) {
                    product[field] = value;
                }
                return product;
            })}
        });
    }

    render() {
        return (<Router>
            <div>
                <div className="container">
                    <nav className="navbar navbar-default">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <Link className="navbar-brand" to="/">Sorting List</Link>
                            </div>
                            <ul className="nav navbar-nav">
                                <li>
                                    <Link to="/">Main</Link>
                                </li>
                                <li>
                                    <Link to="/about">About</Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <Switch>
                    <Route exact path="/" render={(props) => (
                        <MainPage
                            {...props}

                            products = {this.state.products}

                            titleCategoryFilter_WR = {this.titleCategoryFilter}
                            priceFromFilter_WR = {this.priceFromFilter}
                            priceToFilter_WR = {this.priceToFilter}
                            stockedFilter_WR = {this.stockedFilter}
                            colorFilter_WR = {this.colorFilter}
                            checkedFilter_WR = {this.checkedFilter}

                            titleCategoryText = {this.state.titleCategoryText}
                            priceFrom = {this.state.priceFrom}
                            priceTo = {this.state.priceTo}
                            stocked = {this.state.stocked}
                            color = {this.state.color}
                            onlyChecked = {this.state.onlyChecked}


                            nextId = {this.state.nextId}
                            checkedId = {this.state.checkedId}
                            checkRow_WR = {this.checkRow}
                            multiCheckRows_WR = {this.multiCheckRows}

                            countsToShow = {this.state.countsToShow}
                            currentPage = {this.state.currentPage}
                            changeCountsToShow_WR = {this.changeCountsToShow}
                            changeCurrentPage_WR = {this.changeCurrentPage}

                            multiDeleteProducts_WR = {this.multiDeleteProducts}

                            saveNewProduct_WR = {this.saveNewProduct}
                        />
                    )} />
                    <Route exact path="/about" component={AboutPage}/>
                    <Route exact path="/products/:category" render={ (props) => (
                        <CategoryPage
                            {...props}
                            products={this.state.products}
                        />
                )} />
                    <Route exact path="/products/:category/:id" render={(props) => (
                        <ProductPage
                            {...props}
                            products={this.state.products}

                            editProduct_WR = {this.editProduct}
                            SingleDeleteProduct_WR = {this.SingleDeleteProduct}
                        />
                    )}/>
                    <Route component={ErrorPage} />
                </Switch>
            </div>
        </Router>)
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
registerServiceWorker();