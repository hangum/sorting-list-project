import React from 'react';
import {Link} from 'react-router-dom';

import ProductField from "./../../components/ProductField/_index";

/**
 * @state !string activeField
 *
 * @props !array        products
 * @props !fun_wrapper  editProduct_WR
 * @props !fun_wrapper  SingleDeleteProduct_WR
 */
export default class ProductPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activeFiled: null,
            category: this.props.match.params.category
        };

        this.setActiveField = this.setActiveField.bind(this);
        this.updateLinkCategory = this.updateLinkCategory.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    setActiveField(value) {
        this.setState({activeFiled: value});
    }

    updateLinkCategory(category, id){
        //update category
        this.setState({category: category});

        //update url
        let location = {
            pathname: '/products/' + category + '/' + id,
            state: { fromDashboard: true }
        };
        this.props.history.push(location);
    }

    handleDelete(event) {
        //redirect
        let countInCatigory = 0;
        const category = this.state.category;
        this.props.products.forEach(function(product){
            if (product.category === category) {
                ++countInCatigory;
            }
        });
        let location = {};
        if ( countInCatigory > 1) {
            location = {
                pathname: '/products/' + category,
                state: { fromDashboard: true }
            }
        }
        else {
            location = {
                pathname: '/',
                state: { fromDashboard: true }
            }
        }
        this.props.history.push(location);

        //delete
        this.props.SingleDeleteProduct_WR(event.target.value);


    }

    render(){
        const productId = +this.props.match.params.id;

        const products = this.props.products;

        let product = {};
        for (let i=0; i<products.length; i++){
            if (products[i].id === productId) {
                product = products[i];
                break;
            }
        }

        let productInfo = [];
        for ( let field in product){
            if (product.hasOwnProperty(field) && field !== 'id') {
                let openInput = (field === this.state.activeFiled);

                if (field === 'category') {
                    productInfo.push( <ProductField
                        key = {field}

                        value = {product[field]}
                        field = {field}
                        openInput = {openInput}
                        id = {product.id}

                        activeField = {this.state.activeFiled}
                        setActiveField_WR = {this.setActiveField}

                        editProduct_WR = {this.props.editProduct_WR}

                        updateLinkCategory_WR = {this.updateLinkCategory}
                    />);
                }
                else {
                    productInfo.push( <ProductField
                        key = {field}

                        value = {product[field]}
                        field = {field}
                        openInput = {openInput}
                        id = {product.id}

                        activeField = {this.state.activeFiled}
                        setActiveField_WR = {this.setActiveField}

                        editProduct_WR = {this.props.editProduct_WR}
                    />);
                }
            }
        }

        return (<div className="container product-page">
            <Link to="/" style={{marginRight: "24px"}}>
                Main Page
            </Link>
            <Link to={`/products/${this.state.category}`} >
                Category Page
            </Link>

            <hr/>

            <h2>Product info of "{product.name}" <code>#{productId}</code></h2>
            <ul className="list-group">
                {productInfo}
            </ul>

            <button
                className="btn btn-danger"
                onClick = {this.handleDelete}
                value = {product.id}
            >
                Delete Product
            </button>
        </div>);
    }
}