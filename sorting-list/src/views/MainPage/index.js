import React from 'react';

//custom Components
import SearchPanel from './../../components/SearchPanel/_index';
import ProductsTable from './../../components/ProductsTable/_index';
import ModalSuccessSave from './../../components/ModalSuccessSave/_index';


/**
 * @state !boolean      modalSuccessSave
 *
 *
 * @props !array        products
 *
 * @props !fun_wrapper  titleCategoryFilter_WR
 * @props !fun_wrapper  priceFromFilter_WR
 * @props !fun_wrapper  priceToFilter_WR
 * @props !fun_wrapper  stockedFilter_WR
 * @props !fun_wrapper  colorFilter_WR
 * @props !fun_wrapper  checkedFilter_WR
 *
 * @props !fun_wrapper  titleCategoryText
 * @props !string       priceFrom
 * @props !string       priceTo
 * @props !boolean      stocked
 * @props !string       color
 *
 * @props !string       nextId
 * @props !array        checkedId
 * @props !fun_wrapper  checkRow_WR
 * @props !fun_wrapper  multiCheckRows_WR
 *
 * @props !string       countsToShow
 * @props !string       currentPage
 * @props !fun_wrapper  changeCountsToShow_WR
 * @props !fun_wrapper  changeCurrentPage_WR
 *
 * @props !fun_wrapper  multiDeleteProducts_WR
 *
 * @props !fun_wrapper  saveNewProduct_WR
 */
class MainPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {modalSuccessSave: false};

        this.showSuccessSave = this.showSuccessSave.bind(this);
        this.closeSuccessSave = this.closeSuccessSave.bind(this);
    }

    showSuccessSave(){
        this.setState({modalSuccessSave: true});
    }
    closeSuccessSave(){
        this.setState({modalSuccessSave: false});
    }

    render() {
        const products = this.props.products;

        let allColors = []; //color types of products
        products.forEach( function(product) {
            let newColor = true;
            allColors.forEach( function(color) {
                if (product['color'] === color) {
                    newColor = false;
                }
            });
            if (newColor) allColors.push(product['color']);
        });

        return (<div>
            <section className="container">
                <div className="row">
                    <SearchPanel
                        allColors = {allColors}

                        titleCategoryFilter_WR={this.props.titleCategoryFilter_WR}
                        priceFromFilter_WR={this.props.priceFromFilter_WR}
                        priceToFilter_WR={this.props.priceToFilter_WR}
                        stockedFilter_WR={this.props.stockedFilter_WR}
                        colorFilter_WR = {this.props.colorFilter_WR}
                        checkedFilter_WR = {this.props.checkedFilter_WR}

                        titleCategoryText = {this.props.titleCategoryText}
                        priceFrom = {this.props.priceFrom}
                        priceTo = {this.props.priceTo}
                        color = {this.props.color}
                    />
                </div>

                <ProductsTable
                    products={products}
                    nextId = {this.props.nextId}
                    checkedId = {this.props.checkedId}
                    checkRow_WR = {this.props.checkRow_WR}
                    multiCheckRows_WR = {this.props.multiCheckRows_WR}

                    titleCategoryText = {this.props.titleCategoryText}
                    priceFrom = {this.props.priceFrom}
                    priceTo = {this.props.priceTo}
                    stocked = {this.props.stocked}
                    color = {this.props.color}
                    onlyChecked = {this.props.onlyChecked}

                    countsToShow = {this.props.countsToShow}
                    currentPage = {this.props.currentPage}
                    changeCountsToShow_WR = {this.props.changeCountsToShow_WR}
                    changeCurrentPage_WR = {this.props.changeCurrentPage_WR}

                    multiDeleteProducts_WR = {this.props.multiDeleteProducts_WR}

                    saveNewProduct_WR = {this.props.saveNewProduct_WR}

                    showSuccessSave_WR = {this.showSuccessSave}
                />
            </section>
            {this.state.modalSuccessSave &&
            <ModalSuccessSave
                closeSuccessSave_WR = {this.closeSuccessSave}
            />
            }
        </div>
        );
    }
}

export default MainPage;