import React from 'react';
import {Link} from 'react-router-dom';

export default function AboutPage(props){
    return (<div className="container">
        <div className="row">
            <div className="col-md-8 col-md-offset-2 col-sm-offset-0">
                <Link to="/">Back to Main page</Link>
                <h2>About page</h2>
                <p>
                    'Sorting List' app made on React framework with <code>'create-react-app'</code> package.<br/>
                    To start:<br/>
                    <code>
                        cd sorting-list<br/>
                        npm install<br/>
                        npm start
                    </code>
                </p>
                <p>
                    In app realized list of products in table form.
                    Table has pagination; counts of products to show;
                    opportunity to check products for delete; sorting by chosen field;
                    filtration by fields data.
                    Also there is creation form of new product with validation.
                </p>
                <p>
                    List of products is main page in app.
                    Another pages are category, product, about and error pages.
                    Routing made with <code>'react-router-dom'</code> package.
                    On product page realized changing product info or removal of product.
                </p>
            </div>
        </div>
    </div>);
}