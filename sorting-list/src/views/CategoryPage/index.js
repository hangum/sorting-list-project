import React from 'react';
import {Link} from 'react-router-dom';


/**
 * @props !array products
 */
export default function CategoryPage(props){
    const category = props.match.params.category;

    let productsCategory = [];
    for (let i=0; i<props.products.length; i++){
        if (props.products[i].category === category) {
            productsCategory.push(props.products[i]) ;
        }
    }

    return (<div className="container">
        <Link to="/" >
            Main Page
        </Link>

        <hr/>

        <h2>"{category}" products</h2>
        <div className="list-group">
            {productsCategory.map(function(product){
                return (<Link
                        key={product.id}
                        to={`/products/${category}/${product.id}`}
                        className="list-group-item"
                    >
                    <span>{product.name} </span>
                    <code>(#{product.id})</code>
                </Link>
                );
            })}
        </div>
    </div>);
}