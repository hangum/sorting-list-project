import React from 'react';
import { Link } from 'react-router-dom';

import './../../views/ErrorPage/css/custom.css';

function ErrorPage(props){
    return (<div className="container error-page">
        <h1>
            <i className="glyphicon glyphicon-alert" />
            Error
        </h1>
        <h2>Can`t find this page</h2>
        <Link to="/" >
            <button className="btn btn-primary">
                Back to Main page
            </button>
        </Link>
    </div>);
}

export default ErrorPage;